import datetime

import pandas

from db.interfaces import DbCacheInterface


class PandasCache(DbCacheInterface):
    COLUMNS = ['e', 'E', 's', 'a', 'p', 'q', 'f', 'l', 'T', 'm', 'M']

    def __init__(self):
        self._cache = {}

    def add(self, msg):
        pair, type_of_stream = msg["stream"].split("@")
        data = msg["data"]
        data["T"] /= 1000
        _pair = pair.lower()
        self._add_to_df(_pair, type_of_stream, data)
        # print(msg)

    def find_by_last_minutes(self, pair, last_minutes):
        _pair = pair.lower()
        dt = datetime.datetime.now() - datetime.timedelta(minutes=last_minutes)
        t = dt.timestamp()
        df = self._get_df(_pair)
        if isinstance(df, pandas.DataFrame) and not df.empty:
            return df[df["T"] > t]
        return None

    def _get_or_create_df(self, pair):
        if pair in self._cache:
            df = self._cache[pair]
        else:
            df = pandas.DataFrame(columns=self.COLUMNS)
            self._cache[pair] = df
        return df

    def _get_df(self, pair):
        if pair in self._cache:
            df = self._cache[pair]
        else:
            df = None
        return df

    def _add_to_df(self, pair, type_of_stream, data):
        df = self._get_or_create_df(pair)
        df.loc[df.shape[0]] = list(data.values())
