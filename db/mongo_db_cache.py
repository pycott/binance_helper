import datetime

from pymongo import MongoClient

from db.interfaces import DbCacheInterface


class MongoDbCache(DbCacheInterface):
    def __init__(self, mongo_db_url):
        self._mongo_client = MongoClient(mongo_db_url)
        self._mongo_db = self._mongo_client.binance_helper

    def add(self, msg):
        pair, type_of_stream = msg["stream"].split("@")
        data = msg["data"]
        data["T"] /= 1000
        collection = self._get_collection(pair)
        collection.insert_one(data)
        print(msg)

    def find_by_last_minutes(self, pair, last_minutes):
        dt = datetime.datetime.now() - datetime.timedelta(minutes=last_minutes)
        t = dt.timestamp()
        result = list(self._get_collection(pair.lower()).find({"T": {"$gt": t}}))
        return result

    def _get_collection(self, pair):
        return getattr(self._mongo_db, pair)
