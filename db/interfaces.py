import abc


class DbCacheInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def add(self, data):
        pass

    @abc.abstractmethod
    def find_by_last_minutes(self, pair: str, last_minutes: int):
        pass
