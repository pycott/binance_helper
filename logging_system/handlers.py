from logging_system.interfaces import HandlerInterface


class SimpleHandler(HandlerInterface):
    def handle(self, obj: object):
        msg = self.get_formatted_msg(obj)
        self._writer.write(msg)
