from logging_system.interfaces import WriterInterface


class FileWriter(WriterInterface):
    def __init__(self, filename):
        self._filename = filename

    def write(self, message: object):
        with open(self._filename, "a") as f:
            f.write(message)
