import abc


class FormatterInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def format(self, obj: object):
        pass


class WriterInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def write(self, message: object):
        pass


class HandlerInterface(metaclass=abc.ABCMeta):
    def __init__(self, formatter: FormatterInterface, writer: WriterInterface):
        self._formatter = formatter
        self._writer = writer

    def get_formatted_msg(self, obj: object):
        return self._formatter.format(obj)

    @abc.abstractmethod
    def handle(self, obj: object):
        pass
