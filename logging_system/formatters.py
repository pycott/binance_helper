from datetime import datetime

from twisted.python.failure import Failure

from logging_system.interfaces import FormatterInterface


class TwistedFailureFormatter(FormatterInterface):
    def format(self, failure: Failure):
        msg = """
{}
{}
        """.format(datetime.now(), failure.getTraceback())
        return msg
