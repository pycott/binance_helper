from binance.client import Client
from binance.websockets import BinanceSocketManager


class WsAggTradeBinanceDataProducer:
    def __init__(self, pairs, db_cache, creds):
        self._pairs = pairs
        self._db_cache = db_cache
        self._client = Client(creds.get_key(), creds.get_secret())
        self._streams = ["{}@aggTrade".format(pair.lower()) for pair in pairs]
        self._bm = BinanceSocketManager(self._client)
        self._conn_key = self._bm.start_multiplex_socket(self._streams, db_cache.add)

    def run(self):
        print("started")
        self._bm.start()
