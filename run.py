from twisted.internet import task, reactor

from auth.creds import Creds
from db.pandas_cache import PandasCache
from history_analyzer.engine import AnalyzerEngine
from logging_system.formatters import TwistedFailureFormatter
from logging_system.handlers import SimpleHandler
from logging_system.writers import FileWriter
from producers.ws_aggtrade import WsAggTradeBinanceDataProducer
from settings.account_settings import API_KEY, API_SECRET
from settings.analyzers import RUN_ANALYZER_EVERY_SECONDS
from settings.main import PAIRS, ERROR_LOG_PATH


def error_callback(failure):
    writer = FileWriter(ERROR_LOG_PATH)
    formatter = TwistedFailureFormatter()
    twisted_errors_handler = SimpleHandler(formatter, writer)
    print("Something wrong! ;(")
    print("Check error log from '{}' and rerun the program".format(ERROR_LOG_PATH))
    print("Exiting..")
    twisted_errors_handler.handle(failure)
    reactor.stop()
    exit(1)


if __name__ == '__main__':
    db_cache = PandasCache()
    producer = WsAggTradeBinanceDataProducer(PAIRS, db_cache, Creds(API_KEY, API_SECRET))
    analyzer = AnalyzerEngine(PAIRS, db_cache)

    producer.run()

    looping_call = task.LoopingCall(analyzer.run)
    defer = looping_call.start(RUN_ANALYZER_EVERY_SECONDS)
    defer.addErrback(error_callback)
