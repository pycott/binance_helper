class Creds:
    def __init__(self, api_key, api_secret):
        self.__api_key = api_key
        self.__api_secret = api_secret

    def get_key(self):
        return self.__api_key

    def get_secret(self):
        return self.__api_secret
