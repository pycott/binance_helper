import pandas

from history_analyzer.interfaces import AnalyzerInterface
from settings import analyzers as analysers_settings


class RatioOfOpenedAndClosedOrdersAnalyzer(AnalyzerInterface):
    def analyze(self, history: dict) -> bool:
        df = history if isinstance(history, pandas.DataFrame) else pandas.DataFrame(history)
        count_buyers = df[df["m"] == False].shape[0]
        count_sellers = df[df["m"] == True].shape[0]
        expected_ratio = analysers_settings.RatioOfOpenedAndClosedOrdersAnalyzer["ratio"]

        if all((count_buyers, count_sellers)):
            ratio1_res = count_buyers / count_sellers > expected_ratio
            ratio2_res = count_sellers / count_buyers > expected_ratio
            return any((ratio1_res, ratio2_res))
        elif any((count_buyers, count_sellers)):
            return True

        return False
