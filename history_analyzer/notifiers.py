from history_analyzer.interfaces import NotifierInterface


class PrintNotifier(NotifierInterface):
    def notify(self, interesting_pairs: list):
        print("interesting pairs:", interesting_pairs)
