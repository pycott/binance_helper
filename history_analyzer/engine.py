from db.interfaces import DbCacheInterface
from history_analyzer.analyzers import RatioOfOpenedAndClosedOrdersAnalyzer
from history_analyzer.notifiers import PrintNotifier
from settings.analyzers import GET_HISTORY_BY_LAST_MINUTES


class AnalyzerEngine:
    ANALYZERS = (
        RatioOfOpenedAndClosedOrdersAnalyzer(),
    )
    NOTIFIERS = (
        PrintNotifier(),
    )

    def __init__(self, pairs: list, db_cache: DbCacheInterface):
        self._pairs = pairs
        self._db_cache = db_cache

    def run(self):
        interesting_pairs = self._get_interesting_pairs()
        self._notify(interesting_pairs)

    def _get_interesting_pairs(self):
        interesting_pairs = []
        for pair in self._pairs:
            history = self._db_cache.find_by_last_minutes(pair, GET_HISTORY_BY_LAST_MINUTES)
            if history is None:
                continue

            if self._is_interesting_pair(pair, history):
                interesting_pairs.append(pair)
        return interesting_pairs

    def _is_interesting_pair(self, pair, history) -> bool:
        results = []
        for analyzer in self.ANALYZERS:
            results.append(analyzer.analyze(history))
        return all(results)

    def _notify(self, interesting_pairs):
        for notifier in self.NOTIFIERS:
            notifier.notify(interesting_pairs)
