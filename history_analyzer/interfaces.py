import abc


class AnalyzerInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def analyze(self, history: dict) -> bool:
        pass


class NotifierInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def notify(self, interesting_pairs: list):
        pass
